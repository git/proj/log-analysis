"""
Implements storage of collected log files in the local filesystem.
"""

import os, errno

class FilesystemStorage:
	def __init__(self, root):
		self.root = root
		try:
			os.mkdir(root)
		except OSError:
			pass # TODO: proper handling

	def save_file(self, source, filename, data):
		try:
			os.mkdir(os.path.join(self.root, source))
		except OSError:
			pass # TODO: proper handling

		path = os.path.join(self.root, source, os.path.basename(filename)) # TODO: consider adding in date at some point
		with open(path, 'wb') as f:
			f.write(data)
