from contextlib import closing
from database import DatabaseConnection

class PortageDatabaseConnection(DatabaseConnection):
	def __init__(self, db):
		super(PortageDatabaseConnection, self).__init__(db.conn)

	# TODO: consider passing these arguments around in a dictionary or kwargs
	def insert_group_extra(self, group_id, pkg_name, matches, pkg_failed, test_failed, collision, bug_assignee, bug_cc):
		with closing(self.conn.cursor()) as c:
			c.execute("insert into `groups_portage` values (%s, %s, %s, %s, %s, %s, %s, %s)", (group_id, pkg_name, matches, pkg_failed, test_failed, collision, bug_assignee, bug_cc))
			self.conn.commit()
