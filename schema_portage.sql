create table if not exists `groups_portage` (
	`id` int primary key,
	`pkg_name` varchar(255) not null,
	`matches` int not null,
	`pkg_failed` bool not null,
	`test_failed` bool not null,
	`collision` bool not null,
	`bug_assignee` text not null,
	`bug_cc` text not null
);
