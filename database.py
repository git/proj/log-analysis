from contextlib import closing
import MySQLdb

class DatabaseConnection(object):
	def __init__(self, conn):
		self.conn = conn

	def insert_file(self, path, group_id):
		with closing(self.conn.cursor()) as c:
			c.execute("insert into `files` (`path`, `group_id`) values (%s, %s)", (path, group_id))
			self.conn.commit()
			return c.lastrowid

	def insert_group(self, hostname, name, provider, date):
		with closing(self.conn.cursor()) as c:
			c.execute("insert into `groups` (`hostname`, `name`, `provider`, `date`) values (%s, %s, %s, %s)", (hostname, name, provider, date))
			self.conn.commit()
			return c.lastrowid

	def get_groups(self):
		with closing(self.conn.cursor(MySQLdb.cursors.DictCursor)) as c:
			c.execute("select * from `groups`")
			return c.fetchall()
	
	def get_files(self):
		with closing(self.conn.cursor(MySQLdb.cursors.DictCursor)) as c:
			c.execute("select `files`.* from `files` inner join `groups` on `files`.`group_id` = `groups`.`id` order by `groups`.`date` desc")
			return c.fetchall()

def get_connection(user, passwd, db):
	conn = MySQLdb.connect(user=user, passwd=passwd, db=db)
	return DatabaseConnection(conn)
