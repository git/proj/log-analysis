"""
Simple submission client that forms a correct protobuf message and performs a POST
"""

import submission_pb2, sys, urllib2, os

def send_submission(filenames):
	submission = submission_pb2.Submission()
	submission.provider = "portage"
	submission.group_name = "Manual submission"

	for f in filenames:
		new_file = submission.files.add()
		new_file.filename = os.path.basename(f)
		new_file.data = open(f, 'rb').read()

	request = urllib2.Request('http://[::1]:5000/submit', submission.SerializeToString(), {"Content-Type" : "application/octet-stream"})
	print urllib2.urlopen(request).read()

if __name__ == '__main__':
	if len(sys.argv) < 2:
		sys.stderr.write('usage: ' + sys.argv[0] + ' FILENAMES\n')
		sys.exit(-1)
	
	send_submission(sys.argv[1:])
