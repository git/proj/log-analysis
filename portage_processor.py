import os, re, StringIO, time
from portage_database import PortageDatabaseConnection

class PortageProcessor:
	_r = {
		'warnings' : re.compile(r"(Tinderbox QA Warning!|QA Notice: (Pre-stripped|file does not exist|command not found|USE flag|Files built without respecting|The following files)|linux_config_exists|will always overflow|called with bigger|maintainer mode detected|econf called in src_compile|udev rules should be installed)"),
		'testfailed' : re.compile(r"^ \* ERROR: .* failed \(test phase\):"),
		'failed' : re.compile(r"^ \* ERROR: .* failed"),
		'collision' : re.compile(r"Detected file collision"),
		'maintainer' : re.compile(r"^ \* Maintainer: ([a-zA-Z0-9.@_+-]+)(?: ([a-zA-Z0-9.@_+,-]+))?$"),
		'escapes' : re.compile(r"\x1b\[[^\x40-\x7e]*[\x40-\x7e]")
	}

	def __init__(self, storage):
		self.storage = storage

	def process(self, request, source, db):
		db = PortageDatabaseConnection(db)
		group_id = db.insert_group(source, request.group_name, 'portage', int(time.time()))

		matches = 0
		pkg_failed = False
		test_failed = False
		collision = False
		bug_assignee = 'bug-wranglers@gentoo.org'
		bug_cc = ''

		for f in request.files:
			# TODO: look at proper HTML generation methods:
			# (*) either XHTML via xml.etree
			# (*) or Jinja2 (is it possible to parse and generate in one pass?)
			output = StringIO.StringIO()
			output.write('''\
<!doctype html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="htmlgrep.css">
	</head>
	<body>
		<ol>
''')

			for line in f.data.split("\n"):
				match = False

				line = self._r['escapes'].sub('', line)

				if self._r['warnings'].search(line):
					match = True
				elif self._r['testfailed'].search(line):
					test_failed = True
					match = True
				elif self._r['failed'].search(line):
					pkg_failed = True
					match = True
				elif self._r['collision'].search(line):
					pkg_failed = True
					collision = True
					match = True
				else:
					m = self._r['maintainer'].search(line)
					if m:
						bug_assignee, bug_cc = m.group(1, 2)

				if match:
					matches += 1
					output.write('\t'*3 + '<li class="match">' + line + '</li>\n')
				else:
					output.write('\t'*3 + '<li>' + line + '</li>\n')

			output.write('''\
		</ol>
	</body>
</html>
''')

			self.storage.save_file(source, f.filename, output.getvalue())
			file_id = db.insert_file(os.path.join(source, f.filename), group_id)

		db.insert_group_extra(group_id, 'TODO', matches, pkg_failed, test_failed, collision, bug_assignee, bug_cc)
