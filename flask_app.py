"""
The web application built on Flask is contained within this file.
When run as a script, the Flask development server is started.
"""

import os, socket
import submission_pb2, storage, database
from flask import Flask, request, g, render_template

from portage_processor import PortageProcessor

app = Flask(__name__)
store = storage.FilesystemStorage('logs/')
processors = {'portage' : PortageProcessor(store)} # TODO: initialise from config file

@app.before_request
def before_request():
	g.db = database.get_connection('gsoc', 'gsocpasswd', 'loganalysis') # TODO: get from config file

@app.teardown_request
def teardown_request(exception):
	db = getattr(g, 'db', None)
	if db is not None:
		db.conn.close()

@app.route('/')
def index():
	return render_template('group_list.html', groups=g.db.get_groups())

@app.route('/files')
def file_list():
	return render_template('file_list.html', files=g.db.get_files())

@app.route('/submit', methods=['POST'])
def submit():
	submission = submission_pb2.Submission()
	submission.ParseFromString(request.data)
	source = socket.getfqdn(request.remote_addr) # TODO: is this ok?

	processors[submission.provider].process(submission, source, g.db)
	return ''

if __name__ == '__main__':
	app.run(host='::1', debug=True)
