create table if not exists `files` (
	`id` int primary key auto_increment,
	`path` text not null,
	`group_id` int not null
);

create table if not exists `groups` (
	`id` int primary key auto_increment,
	`hostname` varchar(255) not null,
	`name` text not null,
	`provider` varchar(16) not null,
	`date` int not null
);
