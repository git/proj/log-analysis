.PHONY: all protobufs clean

all: protobufs

protobufs: submission.proto
	protoc --python_out=. $^

clean:
	rm -f submission_pb2.py
	rm -f *.pyc
